#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

typedef enum {
	true, false
} bool;

int max_index = 9;

int seed = 0;

int* generate( length) {
	int *pass = (int*) malloc(length * sizeof(int));
	srand((unsigned int) time(NULL ) + seed++);
	int i;
	for (i = 0; i < length; i++) {
		pass[i] = rand() % max_index;
	}
	return pass;
}

bool equal(int *pass, int *new_pass, int length) {
	int i;
	for (i = 0; i < length; i++) {
		if (pass[i] != new_pass[i])
			return false;
	}
	return true;
}

int try_pass(int *pass, int length) {
	bool state = false;
	int i = 0;
	int c_clock = clock();
//	struct timespec start, stop;
	int* new_pass = (int*) malloc(length * sizeof(int));
	int j, k;
	for (j = 0; j < length; j++)
		new_pass[j] = 0;

	do {
//		printf(" [");
//		for (k = 0; k < length; k++)
//			printf("%d", new_pass[k]);
//		printf("] ");
		if (equal(pass, new_pass, length) == true)
			break;

		i = length - 1;

		while ((i >= 0) && (new_pass[i] == max_index)) {
			new_pass[i--] = 0;
		}
		if (i >= 0)
			new_pass[i]++;
		else
			state = true;

	} while (state != true);
//	clock_gettime(CLOCK_REALTIME, &stop);
//	return (stop.tv_sec - start.tv_sec)
//			+ (stop.tv_nsec - start.tv_nsec) / 1000000000.;
	return clock() - c_clock;
}

int main(int argc, char **argv) {
	int *pass_generated;

	int* atempts = (int*) malloc(4 * sizeof(int));
	printf("%-15s%-15s%-15s%-15s%-15s%-15s\n", "symbols", "time1 ms", "time2 ms",
			"time3 ms", "time4 ms", "average ms");
	int i, j;
	for (j = 1; j < 10; j++) {
		for (i = 0; i < 4; i++) {
			pass_generated = generate(j);
			atempts[i] = try_pass(pass_generated, j);
			free(pass_generated);
		}
		printf("%-15d%-15d%-15d%-15d%-15d%-15d\n", j, atempts[0], atempts[1],
				atempts[2], atempts[3],
				(atempts[0] + atempts[1] + atempts[2] + atempts[3]) / 4);
	}
	return 1;
}

